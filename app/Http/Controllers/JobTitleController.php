<?php

namespace App\Http\Controllers;

use App\Models\JobTitle;
use Illuminate\Http\Request;

class JobTitleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list()
    {
        return response()->json([
            'message' => 'Job Title list',
            'data'  => JobTitle::all(),
            'error' => false
        ], 200);
    }
}
