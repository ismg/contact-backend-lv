<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use Illuminate\Http\Request;

use Yajra\DataTables\DataTables;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request)
    {
        $keyword = $request->keyword;
        $count = Contact::with('jobTitles');
        if($keyword != null)
        {
            $count = $count->orWhereHas('jobTitles', function($query) use($keyword) {
                return $query->where('title', 'like', '%' . $keyword . '%');
            })
            ->orWhere('name', 'like', '%' . $keyword . '%')
            ->orWhere('email', 'like', '%' . $keyword . '%');
        }
        $count = $count->count();

        
        $contacts = Contact::with('jobTitles');
        if($keyword != null)
        {
            $contacts = $contacts->orWhereHas('jobTitles', function($query) use($keyword) {
                return $query->where('title', 'like', '%' . $keyword . '%');
            })
            ->orWhere('name', 'like', '%' . $keyword . '%')
            ->orWhere('email', 'like', '%' . $keyword . '%');
        }
        $contacts = $contacts->offset($request->perPage * ($request->page - 1))
                    ->limit($request->perPage)
                    ->orderBy('id', 'desc')
                    ->get();

        return Datatables::of($contacts)->setTotalRecords($count)->toJson();
    }

    /**
     * store the form data for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'job_title_ids' => 'required|array',
            'name' => 'required|string',
            'email' => 'required|email',
        ]);

        if ($validator->fails()) 
        {
            return response()->json([
                'errors' => $validator->errors(),
                'error' => true
            ], 403);
        }

        $request_array = $request->all();
        $job_title_ids =$request_array['job_title_ids'];
        unset($request_array['job_title_ids']);

        $contact = Contact::create($request_array);

        $contact->jobTitles()->sync($job_title_ids);

        return response()->json([
            'message' => 'Contact created successfully',
            'error' => false
        ], 200);
    }
}
