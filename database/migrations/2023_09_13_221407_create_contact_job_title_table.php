<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactJobTitleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contact_job_title', function (Blueprint $table) {
            $table->unsignedBigInteger('contact_id')->nullable()->default(NULL);
            $table->foreign('contact_id')->references('id')->on('contacts');
            $table->unsignedBigInteger('job_title_id')->nullable()->default(NULL);
            $table->foreign('job_title_id')->references('id')->on('job_titles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contact_job_titles');
    }
}
