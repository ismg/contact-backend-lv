<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class JobTitleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $job_titles = ["CEO", "CIO", "CTO", "DEV", "MNG"];

        foreach ($job_titles as $key => $job_title)
        {
            \DB::table('job_titles')->insert([
                'title' => $job_title,
            ]);
        }
    }
}
