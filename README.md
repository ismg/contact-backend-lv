# Software Requirement
PHP 7.4, Composer
# Installation of contact frontend app

1. Please complete above software installation.

2. clone project from gitlab link
[https://gitlab.com/ismg/contact-backend-lv.git](https://gitlab.com/ismg/contact-backend-lv.git)
### `git clone https://gitlab.com/ismg/contact-backend-lv.git`

3. Go to project folder. open the cmd.

4. Type following cmd for composer package installation
### `composer install`

5. Then, create copy of .env.example for .env.
### `cp .env.example .env`

6. Please add database credential into .env file and create database "contacts_db"

7. Run migration and seeder.
### `php artisan migrate --seed`

6. Open cmd and run project
### `php artisan serve`